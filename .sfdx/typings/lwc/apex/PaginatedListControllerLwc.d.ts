declare module "@salesforce/apex/PaginatedListControllerLwc.getAccountsPaginated" {
  export default function getAccountsPaginated(param: {pageSize: any, pageToken: any}): Promise<any>;
}
