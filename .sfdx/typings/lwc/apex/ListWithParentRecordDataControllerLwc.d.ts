declare module "@salesforce/apex/ListWithParentRecordDataControllerLwc.getAccounts" {
  export default function getAccounts(): Promise<any>;
}
