declare module "@salesforce/apex/ViewRelatedRecordsControllerLwc.getRelatedContacts" {
  export default function getRelatedContacts(param: {searchId: any}): Promise<any>;
}
