declare module "@salesforce/apex/RecordPagesController.getSingleAccountViaSOQL" {
  export default function getSingleAccountViaSOQL(): Promise<any>;
}
declare module "@salesforce/apex/RecordPagesController.getSingleContactViaSOQL" {
  export default function getSingleContactViaSOQL(): Promise<any>;
}
