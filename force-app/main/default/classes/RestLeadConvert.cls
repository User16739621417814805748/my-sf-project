/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 02-06-2023
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
@RestResource(urlMapping='/Lead/*')
global with sharing class RestLeadConvert {            

@HttpGet
global static String doGet() {
    String ret = 'fail';
    RestRequest req = RestContext.request;
    RestResponse res = RestContext.response;
    String leadId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);              
    Database.LeadConvert lc = new Database.LeadConvert();
    Lead myLead  = [SELECT Id FROM Lead LIMIT 1];
    lc.setLeadId(myLead.Id);

    LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
    lc.setConvertedStatus(convertStatus.MasterLabel);           
    Database.LeadConvertResult lcr ;
    try{
        lcr = Database.convertLead(lc);
        system.debug('*****lcr.isSuccess()'+lcr.isSuccess());            
        ret = 'ok';
    }
    catch(exception ex){
        system.debug('***NOT CONVERTED**');           
    }
    return ret;
}
}