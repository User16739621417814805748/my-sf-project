/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 01-13-2023
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public with sharing class FutureClass {
   @future
   public static void futureMethod(Id accId){
    System.debug('I am in the future : ' + accId);
   }
}