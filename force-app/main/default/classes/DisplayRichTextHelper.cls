/**
 * @description       : 
 * @author            : Hemdene Ben Hammouda
 * @group             : 
 * @last modified on  : 11-29-2021
 * @last modified by  : Hemdene Ben Hammouda
**/
public with sharing class DisplayRichTextHelper {
    
    @AuraEnabled
    public static Attachment generatePDF(String wrapperList){
        List<PdfWrapper> wrapper = (List<PdfWrapper>)System.JSON.deserialize(wrapperList, List<PdfWrapper>.class);
        System.debug('## Wrapper List : '+wrapper);
        Pagereference pg = Page.renderAsPDF;
        // String nameText = (String)wrapperList.get('valueText');
        // String displayText = (String)wrapperList.get('nameText');
        String nameText = (String)wrapper[0].valueText;
        String displayText = (String)wrapper[0].nameText;
        pg.getParameters().put('displayText', displayText);
        pg.getParameters().put('nameText', nameText);

        Contact con = new Contact(Id='0037Q000002rXaBQAU');
        Attachment objAttachment = new Attachment();
        objAttachment.Name = 'J2S.pdf';
        objAttachment.ParentId = con.Id;
        objAttachment.Body = pg.getContentaspdf();   
        objAttachment.IsPrivate = false;
        insert objAttachment;
        return objAttachment;
    }
public class PdfWrapper{
    public String nameText {get; set;}
    public String valueText {get; set;}
}
}